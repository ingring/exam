#!/bin/bash
cd ~/git/exam

#in case the local repository is not up to date
git pull

#runs the scripts consecutively since they depend on each other
./scrape.sh
./page.sh
./overview.sh


#to generate the overview page on the fly using CGI via fcgiwrap
#I copy the htmlfile.txt and place it inside the same directory as overview.cgi, remove if it already exist
rm -f /usr/lib/cgi-bin/htmlfiles.txt
sudo cp -r ~/git/exam/htmlfiles/htmllist.txt /usr/lib/cgi-bin


#remove temporary files and directories
rm date.txt
rm -r newsscraping
rm htmlfiles/htmllist.txt


#push every changes to git
./gitrepo.sh
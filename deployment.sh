#!/bin/bash

#installing and activate git, nginx and fcgi
#git
sudo apt install git

#nginx and fcgi
sudo apt-get install nginx
sudo etc/init.d/nginx start
sudo apt install fcgiwrap
sudo systemctl enable fcgiwrap
sudo systemctl start fcgiwrap


mkdir ~/git
cd ~/git

#fetching the git repository from gitlab
git clone https://exam:ZqPZxzPx4fCo3GXtTpHJ@gitlab.stud.idi.ntnu.no/ingring/exam.git


#copying the systemd configuration files and store them inside /etc/systemd/user
sudo cp -r ~/git/exam/config/schedule-exam.service /etc/systemd/user
sudo cp -r ~/git/exam/config/schedule-exam.timer /etc/systemd/user

#enabling the scripts in the terminal inside /etc/systemd/user:
cd /etc/systemd/user
systemctl --user enable schedule-exam.service
systemctl --user start schedule-exam.service
systemctl --user enable schedule-exam.timer
systemctl --user start schedule-exam.timer
cd ..


#copy the default, fcgiwrap.conf and overview.cgi file and store it inside the right directory as well as activate it
#first remove the default file if it already exists
sudo rm -f /etc/nginx/sites-available/default
sudo cp -r ~/git/exam/config/default /etc/nginx/sites-available
sudo cp -r ~/git/exam/config/fcgiwrap.conf /etc/nginx

sudo nginx -t
sudo nginx -s reload

sudo mkdir -vp /usr/lib/cgi-bin
sudo cp -r ~/git/exam/overview.cgi /usr/lib/cgi-bin
sudo chmod +x -v /usr/lib/cgi-bin/overview.cgi


#making all the scripts available:
cd ~/git/exam
chmod +x scrape.sh
chmod +x page.sh
chmod +x overview.sh
chmod +x main.sh
chmod +x gitrepo.sh
chmod +x deployment.sh
#!/bin/bash

#cd newsscraping
mkdir -p htmlfiles

#makes the html page only to the news who where made the same date as the scrape script
date=$(cat newsscraping/date.txt)
mkdir htmlfiles/${date}


#loops through all the txt files in the directory, and makes a html page to every txt file in the directory
count=0
for file in newsscraping/${date}/*.txt
do
    count=$((count+1))
    urlnews=$(cat ${file} | head -n 1)
    title=$(cat ${file} | sed -n '2p')
    imglink=$(cat ${file} | sed -n '3p')
    date=$(cat ${file} | sed -n '4p')
    summary=$(cat ${file} | sed -n '5p')
    page=$(echo "${title}" | sed -E 's/:.*//')
    filename="news${count}-${page}.html"
    path="htmlfiles/${date}/${filename}"
    
    echo "<!DOCTYPE html>" > ${path}
    echo "<html>" >> ${path}
    echo "<head>" >> ${path}
    echo "<meta charset=\"UTF-8\">" >> ${path}
    echo "<title>${title}</title>" >> ${path}
    echo "</head>" >> ${path}
    echo "<body>" >> ${path}
    echo "<h1>${title}</h1>" >> ${path}
    echo "<img src=\"${imglink}\">" >> ${path}
    echo "<p>Fetched on ${date} <a href=\"${urlnews}\">at ${page}</a></p>" >> ${path}
    echo "Go back to <a href=\"/index.html\">overview page</a>" >> ${path}
    echo "</body>" >> ${path}
done
#!/bin/bash

#reverses the list of all the html files so the newest html goes on the top
reversed=$(cat htmllist.txt | sort -r | sed -E 's|htmlfiles|http://localhost/htmlfiles|g')

echo "content-type: text/html"
echo ""
echo "<!DOCTYPE html>"
echo "<head><title>index.html</title>"
echo "<meta charset=\"UTF-8\"></head>"
echo "<body>"
echo "<h1>Overview of recent news</h1>"
echo "<ul>"
echo "${reversed}"
echo "</ul>"
echo "</body>"
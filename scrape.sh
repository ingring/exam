#!/bin/bash

#making a directory if it does not exist already
mkdir -p newsscraping
cd newsscraping

#Making a file of today's date so the other scripts can use the same time that the scrape script uses
date +"%Y-%m-%d-%H%M" > date.txt
date=$(cat date.txt)

mkdir ${date}
cd ${date}


#scrapes the three newest articles from...
#tv2.no/nyheter
curl "https://www.tv2.no/nyheter" > newstv2
urlnewstv2=$(cat newstv2 | grep -E '*"article__link" href="/nyheter/"*' | head -n3 | sed -E 's|<a class="article__link" href=|https://www.tv2.no|g' | sed -E 's|>||g' | sed -E 's/ //g' | sed -E 's/"//g')

#nrk.no/innlandet
curl "https://www.nrk.no/innlandet/" > newsnrk
urlnewsnrk=$(cat newsnrk | grep -E '<a class="autonomous" href="https://www.nrk.no/innlandet/.*' | head -n3 | sed 's|^.*https://|https://|g' | sed 's|".*><!--.*$||g' | sed 's/" data-id=.*//g')

#tv2sport, but only on days with even numbers
day=$(date +%d)
if [ $((10#${day}%2)) -eq 0 ];
then
    curl "https://www.tv2.no/sport" > newstv2sport
    urlnewstv2sport=$(cat newstv2sport | grep -E '.*"article__link" href="/sport/.*"' | head -n3 | sed -E 's|<a class="article__link" href=|https://www.tv2.no|g' | sed -E 's|>||g' | sed -E 's/ //g' | sed -E 's/"//g')
fi


#loops through the variables that keeps the three newest articles from the different pages
count=0
for url in ${urlnewstv2} ${urlnewsnrk} ${urlnewstv2sport}
do
    count=$((count+1))
    
    curl ${url} > article
    title=$(cat article | grep -E '<title>' | head -n1 | sed -E 's|</?title>||g' | sed -E 's/ *//' | sed 's/– NRK.*//')
    summary=$(cat article | grep -E '<meta name="description" content=.*' | sed -E 's/<meta name="description" content="//' | sed -E 's|"/?>||')
    urlimg=$(cat article | grep -E '<meta ((property="og:image:secure_url")|(name="twitter:image:src")) content=' | sed -E 's/^.*https:/https:/' | sed -E 's|" ?/?>||' | head -n1)
    
    
    #Just to get more control of where the article is fetched from so I can use it in the filename and the title
    #The content stored inside the page variable depends on where it was fetched from.
    tv2nyheter="https://www.tv2.no/nyheter/"
    nrkinnlandet="https://www.nrk.no/"
    tv2sport="https://www.tv2.no/sport/"
    
    echo "${url}" > url
    
    if grep -q -E ${tv2nyheter} "url";
    then
        page=$(echo "tv2nyheter")
        
    elif grep -q -E ${tv2sport} "url";
    then
        page=$(echo "tv2sport")
        
    elif grep -q -E ${nrkinnlandet} "url";
    then
        page=$(echo "nrkinnlandet")
    fi
    
    filename="news${count}-${page}.txt"
    
    echo "${url}" > ${filename}
    echo "${page}: ${title}" >> ${filename}
    echo "${urlimg}" >> ${filename}
    echo "${date}" >> ${filename}
    echo "${summary}" >> ${filename}
done


#delete temporary files, delete newstv2sport if it exist
rm article
rm newstv2
rm newsnrk
rm -f newstv2sport
rm url
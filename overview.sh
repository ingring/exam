#!/bin/bash

#a function that makes the links to every html page, stores it in a temporary file and reverse it
function filename () {
    cd htmlfiles
    
    #loops inside every html files in every date folder
    for file in */*.html
    do
        title=$(cat ${file} | grep '<h1>' | sed 's/<h1>//' | sed 's|</h1>||')
        folder=$(pwd ${file} | sed 's|/home/pi/git/exam/||')
        echo "<li><a href='${folder}/${file}'>${title}</li>" >> htmllist.txt
    done
    
    #reverses the list of all the html files so the newest html goes on the top
    reversed=$(cat htmllist.txt | sort -r)
    echo "${reversed}"
}

echo "<!DOCTYPE html>" > index.html
echo "<head><title>index.html</title>" >> index.html
echo "<meta charset=\"UTF-8\"></head>" >> index.html
echo "<body>" >> index.html
echo "<h1>Overview of recent news</h1>" >> index.html
echo "<ul>" >> index.html
filename >> index.html
cd ..
echo "</ul>" >> index.html
echo "</body>" >> index.html